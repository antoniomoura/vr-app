#ifndef SCENARIO_H
#define SCENARIO_H

#include "application/gmlibwrapper.h"

// qt
#include <QObject>
#include <QQuickItem>

class mybsplinecurve;

namespace GMlib {
class Visualizer;
}

class Scenario : public GMlibWrapper {
  Q_OBJECT
public:
  using GMlibWrapper::GMlibWrapper;

  void initializeScenario() override;
  void cleanupScenario() override;

  void setColorGauge(QQuickItem *gauge);

public slots:
  void callDefferedGL();
  void onVisualizerSelected(const QString &name);

private:
  QQuickItem *_color_gauge;
  mybsplinecurve *_curve;
};

#endif // SCENARIO_H
