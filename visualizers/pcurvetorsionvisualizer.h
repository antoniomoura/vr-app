/**********************************************************************************
**
** Copyright (C) 1994 Narvik University College
** Contact: GMlib Online Portal at http://episteme.hin.no
**
** This file is part of the Geometric Modeling Library, GMlib.
**
** GMlib is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** GMlib is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with GMlib.  If not, see <http://www.gnu.org/licenses/>.
**
**********************************************************************************/

#ifndef PPCURVETORSIONVISUALIZER_H
#define PPCURVETORSIONVISUALIZER_H

#include <limits>

#include <gmParametricsModule>

#include <QQuickItem>

using GMlib::Color;
using GMlib::DefaultRenderer;
using GMlib::DVector;
using GMlib::SceneObject;
using GMlib::Vector;
using GMlib::GL::FragmentShader;
using GMlib::GL::VertexShader;

template <typename T, int n>
class PCurveTorsionVisualizer : public GMlib::PCurveVisualizer<T, n> {
  GM_VISUALIZER(PCurveTorsionVisualizer)
public:
  PCurveTorsionVisualizer(QQuickItem *color_gauge)
      : GMlib::PCurveVisualizer<T, n>() {
    _color_gauge = color_gauge;
    initShaderProgram();
    _vbo.create();
  }

  PCurveTorsionVisualizer(std::vector<DVector<Vector<T, 3>>> &p,
                            QQuickItem *color_gauge)
      : GMlib::PCurveVisualizer<T, n>(p) {
    _color_gauge = color_gauge;
    initShaderProgram();
    _vbo.create();
  }

  PCurveTorsionVisualizer(const PCurveTorsionVisualizer<T, n> &copy)
      : GMlib::PCurveVisualizer<T, n>(copy) {
    _color_gauge = copy._color_gauge;
    initShaderProgram();
    _vbo.create();
  }

  virtual ~PCurveTorsionVisualizer() {}

  void render(const GMlib::SceneObject *obj,
              const GMlib::DefaultRenderer *renderer) const override {
    const GMlib::Camera *cam = renderer->getCamera();
    const GMlib::HqMatrix<float, 3> &mvpmat =
        obj->getModelViewProjectionMatrix(cam);

    _prog.bind();
    _prog.uniform("u_mvpmat", mvpmat);

    GMlib::GL::AttributeLocation pos_loc = _prog.getAttributeLocation("aPos");
    GMlib::GL::AttributeLocation curv_loc =
        _prog.getAttributeLocation("aTorsionLevel");

    _vbo.bind();

    _vbo.enable(pos_loc, 3, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat),
                reinterpret_cast<void *>(0));
    _vbo.enable(curv_loc, 1, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat),
                reinterpret_cast<void *>(3 * sizeof(GLfloat)));

    GL_CHECK(::glLineWidth(3));
    GL_CHECK(::glDrawArrays(GL_LINE_STRIP, 0, _no_vertices));

    _vbo.unbind();
    _prog.unbind();
  }

  void replot(const std::vector<DVector<Vector<T, n>>> &p, int m, int d,
              bool closed) override {
    _no_vertices = p.size();

    updateTorsions(p);

    _color_gauge->setProperty("max_value", _max_torsion);
    _color_gauge->setProperty("min_value", _min_torsion);

    fillVBO(p);
  }

  void update() override {
    _no_vertices = (*(this->_p)).size();
    replot(*(this->_p), 0, 0, false);
  }

protected:
  GMlib::GL::Program _prog;
  GMlib::GL::VertexBufferObject _vbo;
  int _no_vertices;
  std::vector<T> _torsions;
  T _min_torsion{0};
  T _max_torsion{0};
  QQuickItem *_color_gauge;

private:
  void initShaderProgram() {
    const std::string prog_name = "torsion_shader_prog";
    const std::string vs_name = "torsion_vs";
    const std::string fs_name = "torsion_fs";

    if (_prog.acquire(prog_name)) {
      return; // program already acquired
    }

    // vertex shader
    const std::string vertShaderSource =
        "#version 330 core\n"
        "\n"
        "uniform mat4 u_mvpmat;\n"
        "\n"
        "in vec3 aPos;\n"
        "in float aTorsionLevel;\n"
        "\n"
        "out float vs_fs_TorsionLevel;\n"
        "\n"
        "void main() {\n"
        "    gl_Position = u_mvpmat * vec4(aPos, 1.0);\n"
        "    vs_fs_TorsionLevel = aTorsionLevel;\n"
        "}\n";

    // fragment shader
    const std::string fragShaderSource =
        "#version 330 core\n"
        "\n"
        "in float vs_fs_TorsionLevel;\n"
        "\n"
        "out vec4 fragColor;\n"
        "\n"
        "vec3 hue_to_rgb(float hue)\n"
        "{\n"
        "float R = abs(hue * 6 - 3) - 1;\n"
        "float G = 2 - abs(hue * 6 - 2);\n"
        "float B = 2 - abs(hue * 6 - 4);\n"
        "return vec3(R,G,B);\n"
        "}\n"
        "void main() {\n"
        "    fragColor = vec4(hue_to_rgb((1 -"
        "vs_fs_TorsionLevel) * 0.75), 1.0);\n"
        "}\n";

    bool compile_ok, link_ok;

    VertexShader vertShader;
    vertShader.create(vs_name);
    vertShader.setPersistent(true);
    vertShader.setSource(vertShaderSource);
    compile_ok = vertShader.compile();
    if (not compile_ok) {
      std::cout << "Src:\n" << vertShader.getSource() << "\n\n";
      std::cout << "Error: " << vertShader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    FragmentShader fragShader;
    fragShader.create(fs_name);
    fragShader.setPersistent(true);
    fragShader.setSource(fragShaderSource);
    compile_ok = fragShader.compile();
    if (not compile_ok) {
      std::cout << "Src:\n" << fragShader.getSource() << "\n\n";
      std::cout << "Error: " << fragShader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    // attach, compile, link
    _prog.create(prog_name);
    _prog.setPersistent(true);
    _prog.attachShader(vertShader);
    _prog.attachShader(fragShader);
    link_ok = _prog.link();
    if (!link_ok) {
      std::cout << "Error: " << _prog.getLinkerLog() << std::endl;
    }
    assert(link_ok);
  }

  T computeTorsion(const DVector<Vector<T, n>> &sample) {
    const auto d1 = sample[1];
    const auto d2 = sample[2];
    const auto d3 = sample[3];
    const auto d1xd2 = d1 ^ d2;
    const auto d1xd2abs = d1xd2.getLength();

    return d1xd2 * d3 / (d1xd2abs * d1xd2abs);
  }

  void updateTorsions(const std::vector<DVector<Vector<T, n>>> &p) {
    _torsions.clear();
    _torsions.reserve(_no_vertices);

    T max_torsion = -std::numeric_limits<T>::infinity();
    T min_torsion = std::numeric_limits<T>::infinity();

    for (auto vertex = 0; vertex < _no_vertices; ++vertex) {
      const auto torsion = computeTorsion(p[vertex]);
      if (torsion > max_torsion) {
        max_torsion = torsion;
      }
      if (torsion < min_torsion) {
        min_torsion = torsion;
      }
      _torsions.push_back(torsion);
    }

    _max_torsion = max_torsion;
    _min_torsion = min_torsion;
  }

  void fillVBO(const std::vector<DVector<Vector<T, n>>> &p) {
    _vbo.bufferData(_no_vertices * 4 * sizeof(GLfloat), 0x0, GL_DYNAMIC_DRAW);

    float *ptr = _vbo.mapBuffer<float>();
    for (size_t vertex = 0; vertex < p.size(); ++vertex) {
      *ptr++ = p[vertex][0][0];
      *ptr++ = p[vertex][0][1];
      *ptr++ = p[vertex][0][2];

      *ptr++ = (_torsions[vertex] - _min_torsion) /
               (_max_torsion - _min_torsion);
    }
    _vbo.unmapBuffer();
  }
};

#endif // PCurveCurvatureVisualizer_H
