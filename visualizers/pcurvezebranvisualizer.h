/**********************************************************************************
**
** Copyright (C) 1994 Narvik University College
** Contact: GMlib Online Portal at http://episteme.hin.no
**
** This file is part of the Geometric Modeling Library, GMlib.
**
** GMlib is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** GMlib is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with GMlib.  If not, see <http://www.gnu.org/licenses/>.
**
**********************************************************************************/

#ifndef PPCURVEZEBRANVISUALIZER_H
#define PPCURVEZEBRANVISUALIZER_H

#include <limits>

#include <gmParametricsModule>

using GMlib::Color;
using GMlib::DefaultRenderer;
using GMlib::DVector;
using GMlib::SceneObject;
using GMlib::Vector;
using GMlib::GL::FragmentShader;
using GMlib::GL::VertexShader;

template <typename T, int n>
class PCurveZebraNVisualizer : public GMlib::PCurveVisualizer<T, n> {
  GM_VISUALIZER(PCurveZebraNVisualizer)
public:
  PCurveZebraNVisualizer() : GMlib::PCurveVisualizer<T, n>() {
    initShaderProgram();
    _vbo.create();
  }

  PCurveZebraNVisualizer(std::vector<DVector<Vector<T, 3>>> &p)
      : GMlib::PCurveVisualizer<T, n>(p) {
    initShaderProgram();
    _vbo.create();
  }

  PCurveZebraNVisualizer(const PCurveZebraNVisualizer<T, n> &copy)
      : GMlib::PCurveVisualizer<T, n>(copy) {
    initShaderProgram();
    _vbo.create();
  }

  virtual ~PCurveZebraNVisualizer() {}

  void render(const GMlib::SceneObject *obj,
              const GMlib::DefaultRenderer *renderer) const override {
    const GMlib::Camera *cam = renderer->getCamera();

    const auto up_vector = cam->getGlobalUp();
    const auto dir_vector = cam->getGlobalDir();
    const auto side_vector = cam->getGlobalSide();

    const GMlib::HqMatrix<float, 3> &mvpmat =
        obj->getModelViewProjectionMatrix(cam);

    const GMlib::HqMatrix<float, 3> &mvmat = obj->getModelViewMatrix(cam);

    _prog.bind();
    _prog.uniform("u_mvpmat", mvpmat);
    _prog.uniform("u_mvmat", mvmat);
    _prog.uniform("u_up", up_vector);
    _prog.uniform("u_dir", dir_vector);
    _prog.uniform("u_side", side_vector);

    GMlib::GL::AttributeLocation pos_loc = _prog.getAttributeLocation("aPos");
    GMlib::GL::AttributeLocation normal_loc =
        _prog.getAttributeLocation("aNormal");

    _vbo.bind();

    _vbo.enable(pos_loc, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
                reinterpret_cast<void *>(0));
    _vbo.enable(normal_loc, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
                reinterpret_cast<void *>(3 * sizeof(GLfloat)));

    GL_CHECK(::glLineWidth(3));
    GL_CHECK(::glDrawArrays(GL_LINE_STRIP, 0, _no_vertices));

    _vbo.unbind();
    _prog.unbind();
  }

  void replot(const std::vector<DVector<Vector<T, n>>> &p, int m, int d,
              bool closed) override {
    GM_UNUSED(m);
    GM_UNUSED(d);
    GM_UNUSED(closed);

    _no_vertices = p.size();

    updateNormals(p);

    fillVBO(p);
  }

  void update() override {
    _no_vertices = (*(this->_p)).size();
    replot(*(this->_p), 0, 0, false);
  }

protected:
  GMlib::GL::Program _prog;
  GMlib::GL::VertexBufferObject _vbo;
  int _no_vertices;
  std::vector<Vector<T, n>> _normals;

private:
  void initShaderProgram() {
    const std::string prog_name = "zebra_n_shader_prog";
    const std::string vs_name = "zebra_n_vs";
    const std::string fs_name = "zebra_n_fs";

    if (_prog.acquire(prog_name)) {
      return; // program already acquired
    }

    // vertex shader
    const std::string vertShaderSource =
        "#version 330 core\n"
        "\n"
        "uniform mat4 u_mvpmat;\n"
        "uniform mat4 u_mvmat;\n"
        "\n"
        "in vec3 aPos;\n"
        "in vec3 aNormal;\n"
        "\n"
        "out vec3 vs_fs_Normal;\n"
        "\n"
        "void main() {\n"
        "    gl_Position = u_mvpmat * vec4(aPos, 1.0);\n"
        "    vs_fs_Normal = (u_mvmat * vec4(aNormal, 0.0)).xyz;\n"
        "}\n";

    // fragment shader
    const std::string fragShaderSource =
        "#version 330 core\n"
        "\n"
        "uniform vec3 u_up;\n"
        "uniform vec3 u_dir;\n"
        "uniform vec3 u_side;\n"
        "\n"
        "in vec3 vs_fs_Normal;\n"
        "\n"
        "out vec4 fragColor;\n"
        "\n"
        "void main() {\n"
        "    vec3 unit_dir = normalize(u_dir);\n"
        "    vec3 unit_side = normalize(u_side);\n"
        "    vec3 unit_up = normalize(u_up);\n"
        "    vec3 tan_side_up = vs_fs_Normal - dot(unit_dir, vs_fs_Normal) * "
        "unit_dir;\n"
        "    const float pi = 3.1415926535;\n"
        "    vec3 unit_t_s_u = normalize(tan_side_up);\n"
        "    float x = dot(tan_side_up, unit_side);\n"
        "    float y = dot(tan_side_up, unit_up);\n"
        "    float angle = atan(y, x) + pi;\n"
        "    int bw = int(mod(floor((angle * 180 / pi) / 20), 2));\n"
        "    if (bool(bw)) {\n"
        "        fragColor = vec4(0, 0, 0, 1);\n"
        "    }\n"
        "    else {\n"
        "        fragColor = vec4(1, 1, 1, 1);\n"
        "    }\n"
        "}\n";

    bool compile_ok, link_ok;

    VertexShader vertShader;
    vertShader.create(vs_name);
    vertShader.setPersistent(true);
    vertShader.setSource(vertShaderSource);
    compile_ok = vertShader.compile();
    if (not compile_ok) {
      std::cout << "Src:\n" << vertShader.getSource() << "\n\n";
      std::cout << "Error: " << vertShader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    FragmentShader fragShader;
    fragShader.create(fs_name);
    fragShader.setPersistent(true);
    fragShader.setSource(fragShaderSource);
    compile_ok = fragShader.compile();
    if (not compile_ok) {
      std::cout << "Src:\n" << fragShader.getSource() << "\n\n";
      std::cout << "Error: " << fragShader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    // attach, compile, link
    _prog.create(prog_name);
    _prog.setPersistent(true);
    _prog.attachShader(vertShader);
    _prog.attachShader(fragShader);
    link_ok = _prog.link();
    if (!link_ok) {
      std::cout << "Error: " << _prog.getLinkerLog() << std::endl;
    }
    assert(link_ok);
  }

  Vector<T, n> computeNormal(const DVector<Vector<T, n>> &sample) {
    const auto d1 = sample[1];
    const auto d2 = sample[2];
    const auto d1xd2 = d1 ^ d2;
    const auto tangent = d1 / d1.getLength();
    const auto binormal = d1xd2 / d1xd2.getLength();

    return binormal ^ tangent;
  }

  void updateNormals(const std::vector<DVector<Vector<T, n>>> &p) {
    _normals.clear();
    _normals.reserve(_no_vertices);

    for (auto vertex = 0; vertex < _no_vertices; ++vertex) {
      const auto normal = computeNormal(p[vertex]);
      _normals.push_back(normal);
    }
  }

  void fillVBO(const std::vector<DVector<Vector<T, n>>> &p) {
    _vbo.bufferData(_no_vertices * 6 * sizeof(GLfloat), nullptr,
                    GL_DYNAMIC_DRAW);

    float *ptr = _vbo.mapBuffer<float>();
    for (size_t vertex = 0; vertex < p.size(); ++vertex) {
      *ptr++ = p[vertex][0][0];
      *ptr++ = p[vertex][0][1];
      *ptr++ = p[vertex][0][2];

      *ptr++ = _normals[vertex][0];
      *ptr++ = _normals[vertex][1];
      *ptr++ = _normals[vertex][2];
    }
    _vbo.unmapBuffer();
  }
};

#endif // PCurveCurvatureVisualizer_H
