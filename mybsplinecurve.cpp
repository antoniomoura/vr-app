#include "mybsplinecurve.h"

void mybsplinecurve::registerVisualizer(const std::string &name,
                                        GMlib::Visualizer *vis) {
  _curve_visualizers[name] = vis;
}

void mybsplinecurve::setCurrentVisualizer(const std::string &name) {
  auto new_visualizer_it = _curve_visualizers.find(name);
  if (new_visualizer_it == _curve_visualizers.end()) {
    std::cout << "Unregistered visualizer" << std::endl;
    return;
  }

  if (_current_visualizer) {
    removeVisualizer(_current_visualizer);
  }
  insertVisualizer(new_visualizer_it->second);
  _current_visualizer = new_visualizer_it->second;
  GMlib::PBSplineCurve<float>::sample(_no_samples, _no_derivatives);
}

void mybsplinecurve::sample(int m, int d)
{
  GMlib::PBSplineCurve<float>::sample(m, d);
  _no_samples = m;
  _no_derivatives = d;
}

void mybsplinecurve::localSimulate(double dt) {
  rotate(GMlib::Angle(90) * dt, GMlib::Vector<float, 3>(1.0f, 0.0f, 0.0f));
}
