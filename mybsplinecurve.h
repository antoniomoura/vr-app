#ifndef MYBSPLINECURVE_H
#define MYBSPLINECURVE_H

#include <gmParametricsModule>

#include <unordered_map>

class mybsplinecurve : public GMlib::PBSplineCurve<float> {
public:
  using PBSplineCurve::PBSplineCurve;

  void registerVisualizer(const std::string &name, GMlib::Visualizer *vis);
  void setCurrentVisualizer(const std::string &name);
  void sample(int m, int d) override;

protected:
  void localSimulate(double dt) override;

private:
  std::unordered_map<std::string, GMlib::Visualizer *> _curve_visualizers;
  GMlib::Visualizer *_current_visualizer{nullptr};
  int _no_samples;
  int _no_derivatives;
};

#endif // MYBSPLINECURVE_H
