import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1

import "qrc:/qml/components"

import SceneGraphRendering 1.0

Item {
  id: root

  signal toggleHidBindView

  onToggleHidBindView: hid_bind_view.toggle()

  Renderer {
    id: renderer

    anchors.fill: parent

    rcpair_name: rc_pair_cb.currentText

    ComboBox {
      id: rc_pair_cb
      anchors.top: parent.top
      anchors.left: parent.left
      anchors.margins: 5

      width: 128

      opacity: 0.7

      model: rc_name_model
      textRole: "display"
    }

    Button {
      text: "?"
      anchors.top: parent.top
      anchors.right: parent.right
      anchors.margins: 5

      width: height
      opacity: 0.7

      onClicked: hid_bind_view.toggle()
    }

    HidBindingView {
      id: hid_bind_view
      anchors.fill: parent
      anchors.margins: 50
      visible:false

      states: [
        State{
          name: "show"
          PropertyChanges {
            target: hid_bind_view
            visible: true
          }
        }
      ]

      function toggle() {

        if(state === "") state = "show"
        else state = ""

      }
    }
  }

  Rectangle {
      id: color_gauge
      objectName: "ColorGauge"
      visible: true

      color: Qt.rgba(0.8, 0.8, 0.8, 1)

      anchors.right: parent.right
      anchors.verticalCenter: parent.verticalCenter
      anchors.margins: 25

      width: 80
      height: parent.height * 0.5
      radius: 5
      opacity: 0.8

      property double min_value: 0.0
      property double max_value: 1.0

      Label {
          anchors.bottom: color_img.top
          anchors.horizontalCenter: color_img.horizontalCenter
          anchors.margins: 5

          text: "%1".arg(parent.max_value)
      }

      Image {
          id: color_img
          width: parent.width * 0.25
          height: parent.height * 0.82
          source: "/data/images/color-gauge.png"
          anchors.verticalCenter: color_gauge.verticalCenter
          anchors.horizontalCenter: color_gauge.horizontalCenter
          //anchors.right: parent.right
          anchors.margins: 15
      }

      Label {
          anchors.top: color_img.bottom
          anchors.horizontalCenter: color_img.horizontalCenter
          anchors.margins: 5

          text: "%1".arg(parent.min_value)
      }
  }

  Rectangle {
      id: visualizer_selector
      objectName: "VisualizerSelector"

      color: Qt.rgba(0.8, 0.8, 0.8, 1)

      anchors.right: parent.right
      anchors.bottom: parent.bottom
      anchors.margins: 25

      width: 300
      height: 100
      radius: 5
      opacity: 0.8

      signal visualizerSelected(string name);

      GroupBox {
          title: "Visualizer"
          flat: true
          anchors.fill: parent

          ExclusiveGroup { id: visualizer }

          GridLayout {
              columnSpacing: 20
              columns: 2
              rows: 3
              flow: GridLayout.TopToBottom

              RadioButton {
                  text: "Zebra (Tangent)"
                  exclusiveGroup: visualizer
                  onClicked: {
                      visualizer_selector.visualizerSelected(text);
                  }
              }

              RadioButton {
                  text: "Zebra (Binormal)"
                  exclusiveGroup: visualizer
                  onClicked: {
                      visualizer_selector.visualizerSelected(text);
                  }
              }

              RadioButton {
                  text: "Zebra (Normal)"
                  exclusiveGroup: visualizer
                  onClicked: {
                      visualizer_selector.visualizerSelected(text);
                  }
              }

              RadioButton {
                  text: "Curvature"
                  checked: true
                  exclusiveGroup: visualizer
                  onClicked: {
                      visualizer_selector.visualizerSelected(text);
                  }
              }

              RadioButton {
                  text: "Torsion"
                  exclusiveGroup: visualizer
                  onClicked: {
                      visualizer_selector.visualizerSelected(text);
                  }
              }
          }
      }
  }
}

